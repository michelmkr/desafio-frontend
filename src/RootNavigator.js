import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';
import HomePage   from './pages/HomePage';
import ProdutoPage   from './pages/Produto/ProdutoPage';
import FormularioPage   from './pages/Produto/FormularioPage';



const AppNavigator = createStackNavigator(
    {
        HomePage : { screen : HomePage },
        ProdutoPage : { screen : ProdutoPage },
        FormularioPage: {screen: FormularioPage,}
    },
    {
        initialRouteName : 'HomePage',
        navigationOptions : {
       
        },
        headerMode : 'none'
    }
)

const App = createAppContainer(AppNavigator);
export default App;