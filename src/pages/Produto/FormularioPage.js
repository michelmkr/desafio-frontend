import React, { memo, useState, useEffect } from 'react';
import Background from '../../components/Background';
import Header from '../../components/Header';
import BackButton from '../../components/BackButton';
import TextInput from '../../components/TextInput';
import { View, ScrollView, Alert } from 'react-native';
import Button from '../../components/Button';
import RNPickerSelect from 'react-native-picker-select';
import {get, post, put} from '../../components/LoadData';
const FormularioPage = ({navigation}) => {
    
    const[params, setParams] = useState(navigation.state.params);
    const [codigo, setCodigo] = useState({ value: '', error: '' });
    const [descricao, setDescricao] = useState({ value: '', error: '' });
    const [tipo, setTipo] = useState({ value: '', error: '' });
    const [quantidade, setQuantidade] = useState({ value: '', error: '' });

    const [carregarTipo, setCarregarTipo] = useState([{
        label:'',
        value:''
    }]);

    if(typeof params != 'undefined') {
        useEffect(() => {
            get('produto/'+params.produto)
            .then(res => res.json())
            .then(json => {
                console.log(json);

                setCodigo({ value: json.codigo, error: '' });
                setDescricao({ value: json.descricao, error: '' });
                setTipo({ value: json.tipo, error: '' });
                setQuantidade({ value: parseInt(json.quantidade), error: '' });


            });
        }, [params])
    }

    useEffect(() => {
        get('tipo-produto/')
        .then(res => res.json())
        .then(json => {
            var retorno = json.map(function(item){
                return {label: item.descricao, value:item};
            })
            setCarregarTipo(retorno);
        });
    }, []);


    const salvar = () => {
        var idTipo = '';
        if(typeof tipo.value.id != 'undefined') {
            idTipo = tipo.value.id;
        }
        if(typeof params != 'undefined') {
            put("produto/"+params.produto, {
                "codigo": codigo.value,
                "descricao": descricao.value,
                "tipo":idTipo,
                "quantidade":quantidade.value
            })
            .then(res => res.json())
            .then(json => {
                if(typeof json.errors != 'undefined') {
                    alert("Favor preencha os dados corretmente.");
                } else {
                    alert('Dados alterados com sucesso.');

                } 
            });
            
        } 
        else 
        {
            post("produto/",{
                "codigo": codigo.value,
                "descricao": descricao.value,
                "tipo":idTipo,
                "quantidade":quantidade.value
            })
            .then(res => res.json())
            .then(json => {
                if(typeof json.errors != 'undefined') {
                    alert("Favor preencha os dados corretmente.");
                } else {
                    alert('Dados registrados com sucesso.');
                    setCodigo({ value: '', error: '' });
                    setDescricao({ value: '', error: '' });
                    setTipo({ value: '', error: '' });
                    setQuantidade({ value: '', error: '' });

                } 
            });
        }

    };


    return <Background>
            <BackButton goBack={() => navigation.goBack()} />
            <Header>Produto</Header>
            <ScrollView style={{flex:1, width:'100%'}}>
                <View>
                    <TextInput
                            label="Codigo"
                            returnKeyType="next"
                            value={codigo.value}
                            onChangeText={text => setCodigo({ value: text, error: '' })}
                    />
                    <TextInput
                            label="Descricao"
                            returnKeyType="next"
                            value={descricao.value}
                            onChangeText={text => setDescricao({ value: text, error: '' })}
                    />
                    <RNPickerSelect
                        onValueChange={(value) => setTipo({ value: value, error: '' })}
                        items={carregarTipo}
                    >
                    <TextInput
                            label="Tipo"
                            returnKeyType="next"
                            value={tipo.value.descricao}
                            onChangeText={text => setTipo({ value: text, error: '' })}
                    />
                    </RNPickerSelect>
                    <TextInput
                            label="Quantidade"
                            keyboardType='numeric'
                            returnKeyType="next"
                            value={quantidade.value}
                            onChangeText={text => setQuantidade({ value: text, error: '' })}
                    />
                    <Button mode="outlined" onPress={salvar}>Salvar</Button>
                </View>
            </ScrollView>
            </Background>
     
};

export default memo(FormularioPage);