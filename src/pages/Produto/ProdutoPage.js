import React, { memo, useState, useEffect } from 'react';
import { View, ScrollView, Alert } from 'react-native';
import Background from '../../components/Background';
import {LogoProduto, LogoVenda} from '../../components/Logo';
import Header from '../../components/Header';
import Button from '../../components/Button';
import Paragraph from '../../components/Paragraph';
import {get, remover} from '../../components/LoadData';
import { Card, ListItem, Icon, Text } from 'react-native-elements'
import BackButton from '../../components/BackButton';

const ProdutoPage = ({ navigation }) => {
  
  const [produto, setProduto] = useState([])
  const [carregar, setcarregar] = useState(true)
  const [idProduto, setIdProduto] = useState(null)
  
  const removerProduto = (idProduto) => {
      const sim = () => {
          remover("produto/"+idProduto)
          .then(res => {
              setIdProduto(idProduto);
          });
      }
        Alert.alert('Alerta','Deseja realmente remover este registro ?', [
            {text: 'Sim',onPress: () => {sim()},style: 'edit',},
            {text: 'Não', onPress: () => console.log('Não removeu'),style: 'delete'}
        ],{cancelable: true},);
  };

    const editarProduto = (produto) => {
        
        navigation.navigate('FormularioPage', {'produto':produto});
    }
  const press = (objeto, idProduto) => {
    Alert.alert('Opções','',[
            {text: 'Editar',onPress: () => editarProduto(idProduto),style: 'edit',},
            {text: 'Remover', onPress: () => removerProduto(idProduto),style: 'delete'},
            {text: 'Fechar', onPress: () => console.log('Fechar Pressed')},
            ],{cancelable: true},
    );

  };
  
  const abrirFormulario = () =>
  {
      navigation.navigate("FormularioPage");
  }
  useEffect(() => {
      get('produto/')
        .then(res => {
            res.json().then(json => {
                setProduto(json);
            })
        });
  }, [carregar, idProduto])
  return <Background>
            <BackButton goBack={() => navigation.navigate('HomePage')} />
            <Header>Produtos</Header>
            <Button mode="outlined" onPress={abrirFormulario}>Adicionar</Button>
            <ScrollView style={{flex:1, width:'100%'}}>
                    <View>
                        {
                            produto.map((item, i) => (
                            <ListItem
                                 style={{width:'100%' }}
                                onPress={(p) => press(p, item.id)}
                                key={item.descricao}
                                title={item.descricao}
                                // leftIcon={{ name: '' }}
                                bottomDivider
                                chevron
                                subtitle={item.tipo.descricao}
                            />
                            ))
                        }
                        </View>
            </ScrollView>
        </Background>
};

export default memo(ProdutoPage);
