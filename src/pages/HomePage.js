import React, { memo } from 'react';
import { View } from 'react-native';
import Background from '../components/Background';
import {LogoProduto, LogoVenda} from '../components/Logo';
import Header from '../components/Header';
import Button from '../components/Button';
import Paragraph from '../components/Paragraph';
import { Card, ListItem, Icon } from 'react-native-elements'



const HomePage = ({ navigation }) => {
  
  const press = () => {
    navigation.navigate('ProdutoPage');
  };

  return <Background>
          <Header>Desafio</Header>
            <View style={{flex:1, flexDirection:'column'}}>
              <View >
                <Card title="Produtos">
                  <View key={1} >
                    <Button onPress={press}>
                      <LogoProduto />
                    </Button>
                  </View>
                </Card>
              </View>
            <View >
                <Card title="Venda">
                  <View key={1}>
                    <Button onPress={press}>
                      <LogoVenda />
                    </Button>
                  </View>
                </Card>
            </View>
          </View>
        </Background>
};

export default memo(HomePage);
