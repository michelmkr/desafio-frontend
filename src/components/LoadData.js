import React, { memo } from 'react';
const SERVER = 'http://10.0.0.101:8000/';

const post = (url, parametros = {}) => {
    var body = JSON.stringify(parametros);
    console.log(body);
    return fetch(SERVER+url, {method:'POST', body:body});
};

const remover = (url) => {
    return fetch(SERVER+url,{method:'DELETE'});
};

const put = (url, parametros = {}) => {
    var body = JSON.stringify(parametros);
    console.log(body);
    return fetch(SERVER+url,{method:'PUT', body:body});
};

const get = (url) => {
    return fetch(SERVER+url,{method:'GET'});
};

export {post, remover, put, get};