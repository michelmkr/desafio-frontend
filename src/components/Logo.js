import React, { memo } from 'react';
import { Image, StyleSheet } from 'react-native';

const Logo = () => (
  <Image source={require('../assets/logo.png')} style={styles.image} />
);

const LogoProduto = () => (
  <Image source={require('../assets/logo.png')} style={styles.image} />
);


const LogoVenda = () => (
  <Image source={require('../assets/logo-venda.png')} style={styles.image} />
);

const styles = StyleSheet.create({
  image: {
    width: 72,
    height: 72,
    marginBottom: 0,
  },
});

export default memo(Logo);
export {LogoProduto, LogoVenda};
//export default memo(LogoVenda);
