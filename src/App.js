import { registerRootComponent } from 'expo';
import React, { memo } from 'react';
import RootNavigator from './RootNavigator';
const App = () => {
    return (
        <RootNavigator/>
    );
};
registerRootComponent(App);
